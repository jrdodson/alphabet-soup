#!/usr/bin/env python3
# jrdodson

from enum import Enum
import itertools
import sys

def usage():
	return "Usage: ./solver.py input_file"

def load_puzzle(input_file):
	'''Load puzzle from input file
		and parse into respective object representations.
	'''
	with open(input_file, 'r') as input_file_handle:
		lines = input_file_handle.readlines()
	x_dim = None
	y_dim = None
	puzzle = None
	words = []
	for index, line in enumerate(lines):
		if not x_dim:
			line = line.split("x")
			if len(line) == 2:	
				x_dim = int(line[0]) + 1
				y_dim = int(line[1]) + 1
				puzzle = ''
		elif x_dim and y_dim and index < x_dim:
			line = line.replace(' ', '')
			if len(line) != y_dim:
				raise ValueError("Dimension mismatch: expected length %d, found length %d" % (y_dim, len(line)))
			puzzle += line
		else:
			words += [''.join(line.strip().split(' '))]
	if not x_dim or not y_dim or not puzzle:
		raise ValueError("Couldn't construct puzzle from input file!")
	return (x_dim, y_dim), puzzle, words

def search(dims, puzzle, words):
	'''Search algorithm
		First create coordinates for each character in the puzzle
		
		Then organize puzzle matrix into possible search arrangements (e.g., search left-down-diagonally, search upward, etc.) and
		add to dictionary.

		Finally search through each arrangement and find instance of target word. 
		
	'''
	def format_grid(all_chars, dims, offset, reverse=False):
		formatted = list(itertools.chain.from_iterable([[all_chars[idx] for idx in range(coord, len(all_chars), dims[1] + offset)] for coord in range(dims[1])]))
		if reverse:
			return list(reversed(formatted))
		return formatted	
		
	def construct_matrix(dims, puzzle, all_chars):
		directions = Enum("direction", "up down left right down_right_diag down_left_diag up_right_diag up_left_diag")
		return {
			directions.right: all_chars,
			directions.left: [char for char in reversed(all_chars)],
			directions.down: format_grid(all_chars, dims, 0),
			directions.down_left_diag: format_grid(all_chars, dims, -1),
			directions.down_right_diag: format_grid(all_chars, dims, 1),
			directions.up: format_grid(all_chars, dims, 0, reverse=True),
			directions.up_left_diag: format_grid(all_chars, dims, 1, reverse=True),
			directions.up_right_diag: format_grid(all_chars, dims, -1, reverse=True)
		}
		
	coordinates = [(char, divmod(idx, dims[1])) for idx, char in enumerate(puzzle)]	
	search_matrix = construct_matrix(dims, puzzle, coordinates)
	
	outputs = []
	for _, path in search_matrix.items():
		searchable_string = ''.join([coord[0] for coord in path])
		for word in words:
			if word in searchable_string:
				index_of = searchable_string.index(word)				
				start_coords = path[index_of][1]
				end_coords = path[index_of + len(word) - 1][1]
				outputs += [(word, "%d:%d" % (start_coords[0], start_coords[1]), "%d:%d" % (end_coords[0], end_coords[1]))]
	return outputs

def main(arg):
	dims, puzzle, words = load_puzzle(arg)
	for (word, start, end) in search(dims, puzzle, words):
		print(word, start, end)

if __name__ == "__main__":
	try:
		arg = sys.argv[1]
		main(arg)
	except IndexError as ie:
		print(usage())
