#! /usr/bin/env python3
# jrdodson

from solver import search, load_puzzle
import unittest

DATA_DIR = "test_data"

expected_puzzle = '''HASDF
GEYBH
JKLZX
CVBLN
GOODO
'''
expected_words = ["HELLO", "GOOD", "BYE"]
expected_result_simple =  [('ABC', '0:0', '0:2'), ('AEI', '0:0', '2:2')]
expected_result_less_simple = [('GOOD', '4:0', '4:3'), ('BYE', '1:3', '1:1'), ('HELLO', '0:0', '4:4')]

class MiniSolverTest(unittest.TestCase):
	def test_load_grid(self):
		dims, puzzle, words = load_puzzle("%s/puzzle_2" % DATA_DIR)
		
		self.assertEqual(dims[0] - 1, 5)
		self.assertEqual(dims[1] - 1, 5)
		self.assertEqual(puzzle, expected_puzzle)
		self.assertEqual(words, expected_words)

	def test_search_simple(self):
		dims, puzzle, words = load_puzzle("%s/puzzle" % DATA_DIR)

		words_with_coords = search(dims, puzzle, words)
		self.assertEqual(words_with_coords, expected_result_simple)
		
	def test_search_less_simple(self):
		dims, puzzle, words = load_puzzle("%s/puzzle_2" % DATA_DIR)
		words_with_coords = search(dims, puzzle, words)
		self.assertEqual(words_with_coords, expected_result_less_simple)
		

def main():
	unittest.main()

if __name__ == "__main__":
	main()
